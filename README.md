# capeml-vim

capeml-vim provides Vim functions to support the construction of EML metadata
with the [capeml](https://caplter.github.io/capeml/) and
[capemlGIS](https://github.com/CAPLTER/capemlGIS) packages.

## installation

Use your favorite package manager:

- Vundle:   `Plugin 'https://gitlab.com/caplter/capeml-vim.git'`
- VimPlug:  `Plug 'https://gitlab.com/caplter/capeml-vim.git'`

No additional steps are required.

## overview

capeml-vim generates Rmarkdown chunks with the appropriate naming and syntax to
support the construction of EML using the
[capeml](https://caplter.github.io/capeml/) and
[capemlGIS](https://github.com/CAPLTER/capemlGIS) packages.

For data entities, the desired name of the object is passed to the functions.
Object names are sanitized to ensure that the chunk name does not include
underscores (replaced with hyphen(s)). The resulting output is placed in the
current buffer.

## currently supported capeml and capemlGIS entities

### data entities

- dataTable
- otherEntity
  + For documenting a file (e.g., a PDF) with minimal metadata.
- otherEntity (with read)
  + For documenting an object that is read into the R environment and that will
    accommodate more metadata than a simple file. An example of this might be a
    spatial object for which there is not an EML-compliant CRS/projection. In
    that case, we can make the object an otherEntity instead of a spatialRaster
    or spatialVector with relevant metadata.
- spatialVector

### citations

- usageCitation
- literatureCited

### other

- taxonomicCoverage
- customUnits
- methods (enhanced)
  + Methods that include provenance.


## usage

### data entities 

Data entities require an argument that will be the name of the entity. For
example, calling `:EMLTable my_table` within Vim will yield the following
output in the current buffer (note that R chunk fencing is omitted in this
example). 

Most functions will accommodate many more arguments than are included in these
templates with only the most common arguments included.

```r
# my_table

{r my-table, eval=TRUE}

my_table <- import / generate...process...

try({
    write_attributes(my_table)
    write_factors(my_table)
  })

my_table_desc <- "desc"

my_table_DT <- create_dataTable(
    dfname = my_table,
    description = my_table_desc
    )
```

### other

Unlike data entities, features such as citations, do not require an argument.
For example, calling `:EMLliteraturecited` within Vim will yield the following
output in the current buffer (note that R chunk fencing is omitted in this
example).

```r
# literature-cited

{r literature-cited, eval=TRUE}

middel_2019 <- create_citation("https://doi.org/10.1016/j.scitotenv.2019.06.085")
thorsson <- create_citation("https://doi.org/10.1002/joc.1537")

citations <- list(
    citation = list(
      middel_2019,
      thorsson
      ) # close list of citations
    ) # close citation
```


## License

[MIT](https://choosealicense.com/licenses/mit/)

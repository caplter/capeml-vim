" DATA TABLE -------------------------------------------------------------------

function! s:chunk_dataTable(tableName)

  " Redirect messages to a variable.
  "
  redir => message

  " build create_dataTable chunk
  "

  " ensure chunkname does not have underscores
  let l:chunkName = substitute(a:tableName, "_", "-", "g")

  silent echon "# " a:tableName
  silent echon "\n"
  silent echon "\n"
  silent echon "```{r " l:chunkName ", eval=TRUE}\n"
  silent echon "\n"
  silent echon a:tableName " <- import / generate...process...\n"
  silent echon "\n"
  silent echon "try({\n"
  silent echon "  write_attributes(" a:tableName ")\n"
  silent echon "  write_factors(" a:tableName ")\n"
  silent echon "})\n"
  silent echon "\n"
  silent echon a:tableName "_desc <- \"desc\"\n"
  silent echon "\n"
  silent echon a:tableName "_DT <- create_dataTable(\n"
  silent echon "  dfname = " a:tableName ",\n"
  silent echon "  description = " a:tableName "_desc\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.
  "
  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! -nargs=1 EMLTable call s:chunk_dataTable(<f-args>)


" SPATIAL VECTOR ---------------------------------------------------------------

function! s:chunk_spatialVector(svName)

  " Redirect messages to a variable.
  "
  redir => message

  " build create_spatialVector chunk
  "

  " ensure chunkname does not have underscores
  let l:chunkName = substitute(a:svName, "_", "-", "g")

  silent echon "# " a:svName
  silent echon "\n"
  silent echon "\n"
  silent echon "```{r " l:chunkName ", eval=TRUE}\n"
  silent echon "\n"
  silent echon "# load spatial vector object\n"
  silent echon a:svName " <- sf::read_sf(\n"
  silent echon "  dsn = \"vector_file_directory\",\n"
  silent echon "  layer = \"vector_file_layer_name\"\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "# assign Name to the site identifier variable\n"
  silent echon a:svName " <- " a:svName " %>%\n"
  silent echon "  mutate(Name = site_identifer)\n"
  silent echon "\n"
  silent echon "try({\n"
  silent echon "  write_attributes(" a:svName ")\n"
  silent echon "})\n"
  silent echon "\n"
  silent echon a:svName "_desc <- \"desc\"\n"
  silent echon "\n"
  silent echon a:svName "_SV <- create_spatialVector(\n"
  silent echon "  svname = " a:svName ",\n"
  silent echon "  description = " a:svName "_desc\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.
  "
  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! -nargs=1 EMLSpatialVector call s:chunk_spatialVector(<f-args>)

"
" OTHER ENTITY -----------------------------------------------------------------

function! s:chunk_otherEntityWithRead(oeName)

  " Redirect messages to a variable.
  "
  redir => message

  " build create_otherEntity chunk
  "

  " ensure chunkname does not have underscores
  let l:chunkName = substitute(a:oeName, "_", "-", "g")

  silent echon "# " a:oeName
  silent echon "\n"
  silent echon "\n"
  silent echon "```{r " l:chunkName ", eval=TRUE}\n"
  silent echon "\n"
  silent echon "# example: read a spatial data file\n"
  silent echon a:oeName " <- sf::read_sf(\n"
  silent echon "  dsn = \"vector_file_directory\",\n"
  silent echon "  layer = \"vector_file_layer_name\"\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "try({\n"
  silent echon "  write_attributes(" a:oeName ")\n"
  silent echon "  write_factors(" a:oeName ")\n"
  silent echon "})\n"
  silent echon "\n"
  silent echon a:oeName "_desc <- \"desc\"\n"
  silent echon a:oeName "_additional <- \"additional_information\"\n"
  silent echon "\n"
  silent echon a:oeName "_OE <- create_otherEntity(\n"
  silent echon "  target_file_or_directory = \"path or file\",\n"
  silent echon "  description = " a:oeName "_desc,\n"
  silent echon "  additional_information = " a:oeName "_additional\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.
  "
  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! -nargs=1 EMLOtherEntityWithRead call s:chunk_otherEntityWithRead(<f-args>)



function! s:chunk_otherEntity(oeName)

  " Redirect messages to a variable.
  "
  redir => message

  " build create_otherEntity chunk
  "

  " ensure chunkname does not have underscores
  let l:chunkName = substitute(a:oeName, "_", "-", "g")

  silent echon "# " a:oeName
  silent echon "\n"
  silent echon "\n"
  silent echon "```{r " l:chunkName ", eval=TRUE}\n"
  silent echon "\n"
  silent echon a:oeName "_desc <- \"desc\"\n"
  silent echon a:oeName "_additional <- \"additional_information\"\n"
  silent echon "\n"
  silent echon a:oeName "_OE <- create_otherEntity(\n"
  silent echon "  target_file_or_directory = \"path or file\",\n"
  silent echon "  description = " a:oeName "_desc,\n"
  silent echon "  additional_information = " a:oeName "_additional\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "```"

  "
  " Turn off redirection.
  "
  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! -nargs=1 EMLOtherEntity call s:chunk_otherEntity(<f-args>)


" taxonomic coverage -----------------------------------------------------------

function! s:chunk_taxonomic_coverage()

  " redirect messages to a variable.

  redir => message

  " build taxonomic coverage chunk

  silent echon "## taxonomic coverage\n"
  silent echon "\n"
  silent echon "Taxonomic coverage(s) are constructed using EDI's taxonomyCleanr tool suite.\n"
  silent echon "\n"
  silent echon "*Note* that the `taxa_map.csv` built with the `create_taxa_map()` function and resolving taxonomic IDs (i.e., `resolve_comm_taxa()`) only needs to be run once, a potentially long process, per version/session -- the taxonomicCoverage can be built as many times as needed with `resolve_comm_taxa()` once the `taxa_map.csv` has been generated and the taxonomic IDs resolved.\n"
  silent echon "\n"
  silent echon "```{r taxonomic-coverage, eval=TRUE}\n"
  silent echon "\n"
  silent echon "library(taxonomyCleanr)\n"
  silent echon "\n"
  silent echon "# taxonomyCleanr requires a path (to build the taxa_map)\n"
  silent echon "my_path <- getwd()\n"
  silent echon "\n"
  silent echon "# create or update map\n"
  silent echon "create_taxa_map(path = my_path, x = data_object, col = \"column_name\")\n"
  silent echon "\n"
  silent echon "# resolve taxa by attempting to match the taxon name (here using ITIS (3))\n"
  silent echon "resolve_sci_taxa(path = my_path, data.sources = 3)\n"
  silent echon "\n"
  silent echon "# build the EML taxonomomic coverage\n"
  silent echon "taxa_coverage <- make_taxonomicCoverage(path = my_path)\n"
  silent echon "\n"
  silent echon "# add taxonomic to the other coverages\n"
  silent echon "coverage$taxonomicCoverage <- taxa_coverage\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.

  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! EMLtaxonomiccoverage call s:chunk_taxonomic_coverage()


" enhanced methods -------------------------------------------------------------

function! s:chunk_enhanced_methods()

  " redirect messages to a variable.

  redir => message

  " build enhanced methods chunk

  silent echon "## methods and provenance\n"
  silent echon "\n"
  silent echon "Use this extended approach of developing methods if provenance data are required or there are multiple methods files, otherwise the `create_dateset()` function will look for a methods.md file in the working directory (or a file path and name can be passed).\n"
  silent echon "\n"
  silent echon "```{r methods-provenance, eval=TRUE}\n"
  silent echon "\n"
  silent echon "# load EDIutils\n"
  silent echon "library(EDIutils)\n"
  silent echon "\n"
  silent echon "# methods from file tagged as markdown\n"
  silent echon "main <- list(description = list(read_markdown(\"methods.md\")))\n"
  silent echon "\n"
  silent echon "# provenance: naip\n"
  silent echon "naip <- emld::as_emld(EDIutils::api_get_provenance_metadata(\"knb-lter-cap.623.1\"))\n"
  silent echon "naip$`@context` <- NULL\n"
  silent echon "naip$`@type` <- NULL\n"
  silent echon "\n"
  silent echon "# provenance: lst\n"
  silent echon "landSurfaceTemp <- emld::as_emld(EDIutils::api_get_provenance_metadata(\"knb-lter-cap.677.1\"))\n"
  silent echon "landSurfaceTemp$`@context` <- NULL\n"
  silent echon "landSurfaceTemp$`@type` <- NULL\n"
  silent echon "\n"
  silent echon "enhancedMethods <- EML::eml$methods(methodStep = list(main, naip, landSurfaceTemp))\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.

  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! EMLenhancedmethods call s:chunk_enhanced_methods()


" custom units -----------------------------------------------------------------

function! s:chunk_custom_units()

  " redirect messages to a variable.

  redir => message

  " build custom units chunk

  silent echon "# custom units\n"
  silent echon "\n"
  silent echon "```{r custom-units, eval=TRUE}\n"
  silent echon "\n"
  silent echon "custom_units <- rbind(\n"
  silent echon "  data.frame(\n"
  silent echon "    id = \"milligramPerKilogram\",\n"
  silent echon "    unitType = \"massPerMass\",\n"
  silent echon "    parentSI = \"gramsPerGram\",\n"
  silent echon "    multiplierToSI = 0.000001,\n"
  silent echon "    description = \"millgram of element per kilogram of material\")\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "unitList <- set_unitList(\n"
  silent echon "  custom_units,\n"
  silent echon "  as_metadata = TRUE\n"
  silent echon ")\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.

  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! EMLcustomunits call s:chunk_custom_units()


" literature cited -------------------------------------------------------------

function! s:chunk_literature_cited()

  " redirect messages to a variable.

  redir => message

  " build literature cited chunk

  silent echon "# literature-cited\n"
  silent echon "\n"
  silent echon "```{r literature-cited, eval=TRUE}\n"
  silent echon "\n"
  silent echon "middel_2019 <- create_citation(\"https://doi.org/10.1016/j.scitotenv.2019.06.085\")\n"
  silent echon "thorsson <- create_citation(\"https://doi.org/10.1002/joc.1537\")\n"
  silent echon "\n"
  silent echon "citations <- list(\n"
  silent echon "  citation = list(\n"
  silent echon "    middel_2019,\n"
  silent echon "    thorsson\n"
  silent echon "  ) # close list of citations\n"
  silent echon ") # close citation\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.

  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! EMLliteraturecited call s:chunk_literature_cited()


" usage citation ---------------------------------------------------------------

function! s:chunk_usage_citation()

  " redirect messages to a variable.

  redir => message

  " build usage citation chunk

  silent echon "# usage citation\n"
  silent echon "\n"
  silent echon "```{r usage-citation, eval=TRUE}\n"
  silent echon "\n"
  silent echon "middel_2019 <- create_citation(\"https://doi.org/10.1016/j.scitotenv.2019.06.085\")\n"
  silent echon "thorsson <- create_citation(\"https://doi.org/10.1002/joc.1537\")\n"
  silent echon "\n"
  silent echon "usages <- list(\n"
  silent echon "  middel_2019,\n"
  silent echon "  thorsson\n"
  silent echon ") # close list of usages\n"
  silent echon "\n"
  silent echon "```"

  " Turn off redirection.

  redir END

  " Place the messages in the destination buffer.
  "
  silent put!=message

endfunction

command! EMLusagecitation call s:chunk_usage_citation()

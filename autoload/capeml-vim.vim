"
" function! capeml-vim#chunk_dataTable(tableName)
" 
"   " Redirect messages to a variable.
"   "
"   redir => message
" 
"   " build create_dataTable chunk
"   "
"   silent echon "# " a:tableName
"   silent echon "\n"
"   silent echon "\n"
"   silent echon "```{r " a:tableName ", eval=TRUE}\n"
"   silent echon "\n"
"   silent echon a:tableName " <- import / generate...process...\n"
"   silent echon "\n"
"   silent echon "write_attributes(" a:tableName ")\n"
"   silent echon "write_factors(" a:tableName ")\n"
"   silent echon "\n"
"   silent echon a:tableName "_desc <- \"desc\"\n"
"   silent echon "\n"
"   silent echon a:tableName "_DT <- create_dataTable(dfname = " a:tableName ",\n"
"   silent echon "description = " a:tableName "_desc)\n"
"   silent echon "\n"
"   silent echon "```"
" 
"   " Turn off redirection.
"   "
"   redir END
" 
"   " Place the messages in the destination buffer.
"   "
"   silent put!=message
" 
" endfunction
